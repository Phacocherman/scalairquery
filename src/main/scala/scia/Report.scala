package scia

import scala.io.StdIn
import scalikejdbc._
import scalikejdbc.config._

package object Report {
  def display() = {
    println("\nTop 10 countries with highest number of airports:")
    Storage.getTopCountriesAirportsHighest().foreach { x =>
      println(x("name") + " " + x("count"))
    }
    
    println("\nTop 10 countries with lowest number of airports:")
    Storage.getTopCountriesAirportsLowest().foreach { x =>
      println(x("name") + " " + x("count"))
    }

    println("\nType of runways per country:")
    Storage.getRunways().foreach { x =>
      println(x("name") + " surface: " + x("surfaces"))
    }

    println("\nTop 10 most common runways latitude")
    Storage.getTopRunways().foreach { x =>
      println(x("le_ident") + " " + x("count"))
    }
  }
}
