package scia

import scala.annotation.tailrec
import scala.io.Source

package object CsvReader {
  def read(path : String) : List[List[String]] = {
    val lines = Source.fromFile(path).getLines.toList
    val indices = lines.indices.map(_+1)
    val error_logger = { lineno : Int => msg : String => parse_error(lineno, msg, path) }
    lines.zip(indices).map { t => splitLine(t._1, error_logger(t._2)) }.flatten
    // flatten transforms the List[Option[List[String]]] into a List[List[String]], removing None lines (error lines)
  }

  def splitLine(line : String, error_callback : (String) => Unit) = {
    @tailrec
    def field(line : List[Char], acc : List[Char], quote : Option[Char])
      : (Option[String], Option[List[Char]]) = (line, quote) match {
      case (Nil, Some(_))         => {
                                       error_callback("splitLine: unexpected end of line.")
                                       (None, None)                           // line will be ignored
                                     }
      case (Nil, None)            => (Some(acc.reverse.mkString), None)       // end of line
      case (','::tail, None)      => (Some(acc.reverse.mkString), Some(tail)) // end of field (only if no quote)
      case ('\\'::tail, None)     => field(tail, acc, Some('\\'))             // ignore and quote next
      case ('"'::tail, None)      => field(tail, acc, Some('"'))              // ignore and enable quote
      case ('"'::tail, Some('"')) => field(tail, acc, None)                   // ignore and disable quote
      case (c::tail, Some('\\'))  => field(tail, c::acc, None)                // put quoted char and reset quote
      case (c::tail, quote)       => field(tail, c::acc, quote)               // other cases are not special
    }

    @tailrec
    def splitRec(line : List[Char], fields : List[String]) : Option[List[String]] = (field(line, Nil, None)) match {
      case (None, _)                 => None  // return no fields if an error occured
      case (Some(field), None)       => Some((field::fields).reverse)
      case (Some(field), Some(rest)) => splitRec(rest, field::fields)
    }

    splitRec(line.toList, List.empty[String])
  }

  def parse_error(lineno : Int, filename : String, message : String) : Unit = {
    val delim = ": "
    System.err.println(filename + delim + lineno.toString + delim + message)
  }
}
