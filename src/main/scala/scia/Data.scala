package scia

import scalikejdbc._

abstract class Data {
  def reflectData : Class[_ <: Data]

  def fields = reflectData.getDeclaredFields.map { f => (f.getName, f.getType) }

  def toSeq() : Seq[(Symbol, Any)] = {
    fields.map { f => (Symbol(f._1), f._2.cast(reflectData.getDeclaredMethod(f._1).invoke(this))) }
  }
}

case class Airport(
  id : Integer,
  ident : String,
  type_ : String,
  name : String,
  latitude_deg : String,
  longitude_deg : String,
  elevation_ft : String,
  continent : String,
  iso_country : String,
  iso_region : String,
  municipality : String,
  scheduled_service : String,
  gps_code : String,
  iata_code : String,
  local_code : String,
  home_link : String,
  wikipedia_link : String,
  keywords : String
) extends Data
{
  override def reflectData = getClass
}

case class Country(
  id : Integer,
  code : String,
  name : String,
  continent : String,
  wikipedia_link : String,
  keywords : String
) extends Data
{
  override def reflectData = getClass
}

case class Runway(
  id : Integer,
  airport_ref : Integer,
  airport_ident : String,
  length_ft : String,
  width_ft : String,
  surface : String,
  lighted : String,
  closed : String,
  le_ident : String,
  le_latitude_deg : String,
  le_longitude_deg : String,
  le_elevation_ft : String,
  le_heading_degT : String,
  le_displaced_threshold_ft : String,
  he_ident : String,
  he_latitude_deg : String,
  he_longitude_deg : String,
  he_elevation_ft : String,
  he_heading_degT : String,
  he_displaced_threshold_ft : String
) extends Data
{
  override def reflectData = getClass
}
