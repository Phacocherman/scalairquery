package scia.data_helpers

import scia.Runway

package object RunwayHelper extends DataHelper[Runway] {
  override val headers = "id,airport_ref,airport_ident,length_ft,width_ft,surface,lighted,closed,le_ident,le_latitude_deg,le_longitude_deg,le_elevation_ft,le_heading_degT,le_displaced_threshold_ft,he_ident,he_latitude_deg,he_longitude_deg,he_elevation_ft,he_heading_degT,he_displaced_threshold_ft"
  override val fieldsCount = 18

  override def fromList(fields: List[String], error_handler: (String) => Unit) = fields match {
    case (id::arf::ait::lft::wft::sf::lg::cl::lit::lla::llo::lel::lhd::ldt::hit::hla::hlo::hel::hhd::hdt::Nil) => {
      try {
        val idi = id.toInt
        val arfi = arf.toInt
        Some(Runway(idi, arfi, ait, lft, wft, sf, lg, cl, lit, lla, llo, lel, lhd, ldt, hit, hla, hlo, hel, hhd, hdt))
      }
      catch {
        case exn: NumberFormatException => {
          error_handler("RunwayHelper.fromList: invalid format: expected Int for fields id and airport_ref")
          None
        }
      }
    }
    case _ => {
      fieldCountErrorHandler(error_handler)(fields.length)
      None
    }
  }
}

