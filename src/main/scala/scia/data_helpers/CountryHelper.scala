package scia.data_helpers

import scia.Country

package object CountryHelper extends DataHelper[Country] {
  override val headers = "id,code,name,continent,wikipedia_link,keywords"
  override val fieldsCount = 18

  override def fromList(fields: List[String], error_handler: (String) => Unit) = fields match {
    case (id::code::name::continent::wiki::kw::Nil) => {
      try {
        Some(Country(id.toInt, code, name, continent, wiki, kw))
      }
      catch {
        case exn: NumberFormatException => {
          error_handler("CountryHelper.fromList: invalid format: expected Int for field id")
          None
        }
      }
    }
    case _ => {
      fieldCountErrorHandler(error_handler)(fields.length)
      None
    }
  }
}

