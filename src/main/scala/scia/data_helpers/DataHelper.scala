package scia.data_helpers

trait DataHelper[T] {
  val headers: String
  val fieldsCount: Int
  def fromList(fields: List[String], error_handler: (String) => Unit): Option[T]

  def fieldCountErrorHandler(error_handler: (String) => Unit) = {
    (c: Int) => {
      error_handler("Invalid field count (expected: " + fieldsCount.toString + ", got: " + c.toString + ")")
    }
  }
}
