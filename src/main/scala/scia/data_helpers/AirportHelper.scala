package scia.data_helpers

import scia.Airport

package object AirportHelper extends DataHelper[Airport] {
  override val headers = "id,ident,type,name,latitude_deg,longitude_deg,elevation_ft,continent,iso_country,iso_region,municipality,scheduled_service,gps_code,iata_code,local_code,home_link,wikipedia_link,keywords"
  override val fieldsCount = 18

  override def fromList(fields: List[String], error_handler: (String) => Unit) = fields match {
    case (id::ide::typ::nam::lat::lon::el::con::isoc::isor::mun::ss::gps::iata::lcod::hl::wiki::kw::Nil) => {
      try {
        Some(Airport(id.toInt, ide, typ, nam, lat, lon, el, con, isoc, isor, mun, ss, gps, iata, lcod, hl, wiki, kw))
      }
      catch {
        case exn: NumberFormatException => {
          error_handler("AirportHelper.fromList: invalid format: expected Int for field id")
          None
        }
      }
    }
    case _ => {
      fieldCountErrorHandler(error_handler)(fields.length)
      None
    }
  }
}

