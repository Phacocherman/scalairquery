package scia

import scala.io.StdIn
import scalikejdbc._
import scalikejdbc.config._

package object Query {
  def perform(s : String) {
    val res = Storage.getAirportsAndRunways(s)
    if (res.size > 0){
      println("Airports and runways of " + s)
      res.foreach(x => println(x("name") + ": { length: " + x("length_ft")
        + ", width: " + x("width_ft") + ", surface: " + x("surfaces") + " }"))
      println("")
    } else {
      println("[ERROR] Please enter a correct country name/code\n")
    }
  }

  def input() {
    print("Please write country name or code: ")
    val country = scala.io.StdIn.readLine().map(_.toUpper)

    perform(country)
  }
}
