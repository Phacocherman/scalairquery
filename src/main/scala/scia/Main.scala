package scia

import scia.data_helpers._

object Main {
  def main(args: Array[String]) : Unit = {
    // Get db login infos
    print("Enter DB name: ")
    val db = scala.io.StdIn.readLine()
    print("Enter DB username: ")
    val usr = scala.io.StdIn.readLine()

    try {
      // Connect to the db
      Storage.initDatabase(db, usr)

      // Add tables
      Storage.createAirportsTable()
      Storage.createCountriesTable()
      Storage.createRunwaysTable()
    } catch {
      case e: Exception => System.err.println("Cannot connect to pg"); return
    }

    // Load csv data into db
    Utils.loadData("data/airports.csv")
    Utils.loadData("data/countries.csv")
    Utils.loadData("data/runways.csv")

    // User input
    repl(0)

    // Close db connection
    Storage.closeDatabase()
  }

  def repl(c : Int) : Unit = c match {
    case 1 => { Query.input(); repl(0) }
    case 2 => { Report.display(); repl(0) }
    case 3 => return
    case _ => {
      print("Choose an option (1 = Query; 2 = Reports; 3 = Quit): ")
      val input = try {
        scala.io.StdIn.readInt()
      } catch {
        case e: NumberFormatException => 0
      }
      if (input < 1 || input > 3)
        println("[ERROR] Please select a valid option\n")
      repl(input)
    }
 }
} 
