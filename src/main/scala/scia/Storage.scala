package scia

import scalikejdbc._
import scalikejdbc.config._

package object Storage {
  def initDatabase(bddName : String, userName : String) {
    Class.forName("org.postgresql.Driver")
    print("Password: ")
    val pwd = System.console().readPassword().mkString
    DBs.setupAll()
    implicit val session = AutoSession
    ConnectionPool.singleton("jdbc:postgresql:" + bddName, userName, pwd)
  }

  def closeDatabase() {
    DBs.closeAll()
  }

  def addAirports(airports : List[Airport]) = {
    DB localTx  { implicit session =>
      val rows = airports.map(_.toSeq)
      sql"""
      INSERT INTO airports (
        id, ident, type, name, latitude_deg, longitude_deg,
        elevation_ft, continent, iso_country, iso_region,
        municipality, scheduled_service, gps_code, iata_code,
        local_code, home_link, wikipedia_link, keywords)
      VALUES (
        {id}, {ident}, {type_}, {name}, {latitude_deg}, {longitude_deg},
        {elevation_ft}, {continent}, {iso_country}, {iso_region},
        {municipality}, {scheduled_service}, {gps_code}, {iata_code},
        {local_code}, {home_link}, {wikipedia_link}, {keywords})
      ON CONFLICT (id) DO NOTHING;
      """.batchByName(rows : _*).apply()
    }
  }

  def addCountries(countries : List[Country]) = {
    DB localTx  { implicit session =>
      val rows = countries.map(_.toSeq)
      sql"""
      INSERT INTO countries (id, code, name, continent, wikipedia_link, keywords)
      VALUES ({id}, {code}, {name}, {continent}, {wikipedia_link}, {keywords})
      ON CONFLICT (id) DO NOTHING;
      """.batchByName(rows : _*).apply()
    }
  }

  def addRunways(runways : List[Runway]) = {
    DB localTx  { implicit session =>
      val rows = runways.map(_.toSeq)
      sql"""
      INSERT INTO runways (
        id, airport_ref, airport_ident, length_ft, width_ft, surface, lighted, closed,
        le_ident, le_latitude_deg, le_longitude_deg, le_elevation_ft, le_heading_degT,
        le_displaced_threshold_ft,
        he_ident, he_latitude_deg, he_longitude_deg, he_elevation_ft, he_heading_degT,
        he_displaced_threshold_ft)
      VALUES (
        {id}, {airport_ref}, {airport_ident}, {length_ft}, {width_ft}, {surface}, {lighted}, {closed},
        {le_ident}, {le_latitude_deg}, {le_longitude_deg}, {le_elevation_ft}, {le_heading_degT},
        {le_displaced_threshold_ft},
        {he_ident}, {he_latitude_deg}, {he_longitude_deg}, {he_elevation_ft}, {he_heading_degT},
        {he_displaced_threshold_ft})
      ON CONFLICT (id) DO NOTHING;
      """.batchByName(rows : _*).apply()
    }
  }

  def createAirportsTable() = {
    DB localTx { implicit session =>
      sql"""
        drop table if exists airports
      """.execute.apply()
      sql"""
        create table airports (
          id serial not null primary key,
          ident text,
          type text,
          name text,
          latitude_deg text,
          longitude_deg text,
          elevation_ft text,
          continent text,
          iso_country text,
          iso_region text,
          municipality text,
          scheduled_service text,
          gps_code text,
          iata_code text,
          local_code text,
          home_link text,
          wikipedia_link text,
          keywords text
        )
        """.execute.apply()
    }
  }

  def createCountriesTable() = {
    DB localTx { implicit session =>
      sql"""
        drop table if exists countries
      """.execute.apply()
      sql"""
        create table countries (
          id serial not null primary key,
          code text,
          name text,
          continent text,
          wikipedia_link text,
          keywords text
        )
        """.execute.apply()
    }
  }

  def createRunwaysTable() = {
    DB localTx { implicit session =>
      sql"""
        drop table if exists runways
      """.execute.apply()
      sql"""
        create table runways (
          id serial not null primary key,
          airport_ref text,
          airport_ident text,
          length_ft text,
          width_ft text,
          surface text,
          lighted text,
          closed text,
          le_ident text,
          le_latitude_deg text,
          le_longitude_deg text,
          le_elevation_ft text,
          le_heading_degT text,
          le_displaced_threshold_ft text,
          he_ident text,
          he_latitude_deg text,
          he_longitude_deg text,
          he_elevation_ft text,
          he_heading_degT text,
          he_displaced_threshold_ft text
        )
      """.execute.apply()
    }
  }

  def getTopCountriesAirportsHighest() = {
    implicit val session = AutoSession
    sql"""
      select countries.name, count(airports.iso_country) from airports,
      countries
      where countries.code = airports.iso_country
      group by countries.name order by count(airports.iso_country) desc
      limit 10
    """.map(_.toMap).list.apply()
  }
  
  def getTopCountriesAirportsLowest() = {
    implicit val session = AutoSession
    sql"""
      select countries.name, count(airports.iso_country) from airports,
      countries
      where countries.code = airports.iso_country
      group by countries.name order by count(airports.iso_country)
      limit 10
    """.map(_.toMap).list.apply()
  }

  def getRunways() = {
    implicit val session = AutoSession
    sql"""
      select countries.name, array_agg(distinct runways.surface) as surfaces
      from countries, runways, airports
      where runways.airport_ident = airports.ident
            and airports.iso_country = countries.code
      group by countries.name
      order by countries.name
    """.map(_.toMap).list.apply()
  }
 
  def getTopRunways() = {
    implicit val session = AutoSession
    sql"""
      select runways.le_ident, count(runways.le_ident) from runways
      group by runways.le_ident
      order by count(runways.le_ident) desc
      limit 10
    """.map(_.toMap).list.apply()
  }

  def getAirportsAndRunways(s: String) = {
    implicit val session = AutoSession
    val query = {
      if (s.length == 2){
      sql"""
        select airports.name, runways.length_ft, runways.width_ft,
        array_agg(distinct runways.surface) as surfaces
        from airports, runways
        where airports.iso_country = ${s}
              and runways.airport_ident = airports.ident
        group by airports.name, runways.length_ft, runways.width_ft
        order by airports.name
      """
      }
      else {
        sql"""
          select airports.name, runways.length_ft, runways.width_ft,
          array_agg(distinct runways.surface) as surfaces
          from airports, runways, countries
          where countries.name = ${s}
                and countries.code = airports.iso_country
                and runways.airport_ident = airports.ident
          group by airports.name, runways.length_ft, runways.width_ft
          order by airports.name
        """
      }
    }
    query.map(_.toMap).list.apply()
  }
}
