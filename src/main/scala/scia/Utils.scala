package scia

import data_helpers._

package object Utils {
  def loadData(path: String) = {
    println("Loading " + path)
    val rows = CsvReader.read(path)
    val loader: Option[(List[List[String]]) => Unit] = {
      rows.head.mkString(",") match {
       case AirportHelper.headers => Some(loadAirports)
       case CountryHelper.headers => Some(loadCountries)
       case RunwayHelper.headers => Some(loadRunways)
       case _ => System.err.println("loadData: " + path + " headers don't match"); None
      }
    }
    if (loader.isDefined)
      loader.get(rows.tail)
  }

  def loadAirports(rows: List[List[String]]) = {
    val airports = rows.flatMap(AirportHelper.fromList(_, System.err.println))
    Storage.addAirports(airports)
    println("Loaded as airports.\n")
  }

  def loadCountries(rows: List[List[String]]) = {
    val countries = rows.flatMap(CountryHelper.fromList(_, System.err.println))
    Storage.addCountries(countries)
    println("Loaded as countries.\n")
  }

  def loadRunways(rows: List[List[String]]) = {
    val runways = rows.flatMap(RunwayHelper.fromList(_, System.err.println))
    Storage.addRunways(runways)
    println("Loaded as runways.\n")
  }
}
