import sbt._

libraryDependencies ++= Seq(
  "org.scalikejdbc" %% "scalikejdbc"       % "3.2.2",
  "org.scalikejdbc" %% "scalikejdbc-config"       % "3.2.2",
  "com.h2database"  %  "h2"                % "1.4.197",
  "org.postgresql" % "postgresql"           % "42.2.2",
  "org.scalactic" %% "scalactic" % "3.0.5",
  "org.scalatest" %% "scalatest" % "3.0.5" % "test"
)

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "com.scia",
      scalaVersion := "2.12.5",
      version      := "0.1.0-SNAPSHOT"
    )),
    name := "ScalAirQuery"
  )
